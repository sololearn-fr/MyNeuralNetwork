# HowTo Contribute

## Before joining the team

- Take a look at the already known issues
- Fork the project to meet the source code
- Develop your first development issue or enhancement issue
- Commit with meaningful comment
- Test your source code
- Request for a merge on dev branch

Your source code will be analyzed
Please don't spam the project owner to join the team

## After joining the team

For each issue : 
- Create a separate local branch
- Commit with meaningful comment
- Test your source code
- Request for a merge on dev branch