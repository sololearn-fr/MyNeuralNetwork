﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyNeuralNetwork.Classes
{
    public static class ActivationFunctions
    {
        // Step x =  0 if x <  0
        //           1 if x >= 0
        // Ramp x = -1 if x < -1
        //           x if -1 <  x  < 1
        //           1 if x >  1
        // exponential x = e ^ -x
        // sigmoid x = 1 / (1 + e ^ -x)
        // tanh x = (e ^ x - e ^ -x) / (e ^ x + e ^ -x)

        public static double Linear(double value)
        {
            return value;
        }

        public static double DerivativeLinear(double value)
        {
            return 1;
        }

        public static double Exponential(double value)
        {
            return Math.Exp(-value);
        }

        public static double DerivativeExponential(double value)
        {
            return -1 * Math.Exp(-value);
        }

        public static double Sigmoid(double value)
        {
            return 1 / (1 + Math.Exp(-value));
        }

        public static double DerivativeSigmoid(double value)
        {
            double s = Sigmoid(value);
            return s * (1 - s);
        }

        public static double Tanh(double value)
        {
            return Math.Tanh(value);
        }

        public static double DerivativeTanh(double value)
        {
            return 1.0 - value * value;
        }
    }
}