﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyNeuralNetwork.Classes
{
    class Axon
    {
        // Propertites

        /// <summary>
        /// Output value of the neuron (for Forward Propagation)
        /// </summary>
        public double Value { get; set; }

        /// <summary>
        /// Gradient of the neuron (for Backpropagation)
        /// </summary>
        public double Gradient { get; private set; }

        // Association(s)

        public Neuron Neuron { get; private set; }              // connected neuron
        public List<Dendrite> Dendrites { get; private set; }   // connected dendrites

        // Constructor(s)

        public Axon(Neuron neuron)
        {
            Neuron = neuron;
            Dendrites = new List<Dendrite>();
        }

        // Method(s)

        /// <summary>
        /// Get the weighted sum of gradients from the axon and propagate it by the back function
        /// </summary>
        public void BackPropagation()
        {
            // Internal value of the neuron is the weighted sum of the dendrites incomming values

            // Sum the values incoming from our dendrites
            double sum = Dendrites.Sum(dendrite => dendrite.WeightedGradient);

            // Back propagation of the gradient
            Gradient = sum * Neuron.Layer.Network.DerivativeFunction(Value);
        }

        /// <summary>
        /// Initialize the gradient for Backpropagation
        /// </summary>
        /// <param name="expected">Expected value for the axon</param>
        public void BackPropagation(double expected)
        {
            // calculating the delta between the expected value and the axon's value
            double delta = expected - Value;

            // Back propagation of the gradient
            Gradient = delta * Neuron.Layer.Network.DerivativeFunction(Value);
        }
    }
}
