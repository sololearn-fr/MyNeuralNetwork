﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyNeuralNetwork.Classes
{
    class Layer : List<Neuron>
    {
        // Properties

        /// <summary>
        /// Index of the layer in the network
        /// </summary>
        public int Index { get; private set; }


        // Link(s)

        /// <summary>
        /// Network wich the layer belongs
        /// </summary>
        public Network Network { get; private set; }


        // Constructor(s)
        public Layer(Network network, int index, int neuronsCount) : base()
        {
            Console.WriteLine("Creating a new Layer");

            Network = network;
            Index = index;

            // Adding neurons in the layer (n neurons + 1 bias neuron)
            for (int neuronIndex = 0; neuronIndex <= neuronsCount; neuronIndex++)
            {
                Neuron neuron;

                if (index == 0)
                {
                    // First layer
                    // There is no previous layer

                    // Instanciating a new neuron
                    neuron = new Neuron(neuronIndex, this);
                }
                else
                {
                    // Not fist layer
                    // There is a previous layer

                    // Instanciating a new neuron
                    neuron = new Neuron(neuronIndex, this, network.Layers[index - 1]);
                }


                // Adding the neuron in the layer
                // NB : the layer is a List<Neuron> (by inheritance)
                this.Add(neuron);
            }
        }
    }
}
