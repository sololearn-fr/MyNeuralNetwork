﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MyNeuralNetwork.Classes
{
    class Network
    {
        // Private field(s)

        private double recentAverageSmoothingFactor = 100.0;

        // Public Properties

        /// <summary>
        /// Neural Network Topology (number of neurons by layer)
        /// </summary>
        public Topology Topology { get; set; }

        /// <summary>
        /// Overall net learning rate [0.0..1.0]
        /// ( 0.0 : slow learner, 0.2 : medium learner, 1.0 : reckless learner )
        /// </summary>
        public double LearnRate { get; private set; } = 0.2;

        /// <summary>
        /// Momentum :
        /// multiplier for last weight change [0.0..n]
        /// ( 0.0 : no momentum, 0.5 : moderate momentum )
        /// </summary>
        public double Momentum { get; private set; } = 0.5;

        /// <summary>
        /// Max number of training cycles
        /// </summary>
        public double MaxEpochs { get; private set; } = 5000;

        /// <summary>
        /// Average error to reach down
        /// </summary>
        public double TargetedAverageError { get; set; } = 0.08;

        /// <summary>
        ///  Name of the activation function
        /// </summary>
        public string ActivationFunctionName { get; private set; } = "Tanh";

        public delegate double Function(double value);

        /// <summary>
        /// Function that activate the neuron for Forward Propagation
        /// </summary>
        /// <param name="value">the weighted sum of values that are incomming to the neuron by the dendrites</param>
        /// <returns></returns>
        public Function ActivationFunction;

        /// <summary>
        /// Function that activate the neuron for Backpropagation
        /// </summary>
        /// <param name="value">the weighted sum of gradients that are incomming to the neuron by the axon</param>
        /// <returns></returns>
        public Function DerivativeFunction;

        /// <summary>
        /// Current Average Error
        /// </summary>
        public double RecentAverageError { get; private set; }

        /// <summary>
        /// Neural Network output
        /// </summary>
        public List<double> Results
        {
            get
            {
                List<double> results = new List<double>();
                Layer resultLayer = Layers[Layers.Count - 1];
                for (int neuronIndex = 0; neuronIndex < resultLayer.Count - 1; neuronIndex++)
                {
                    results.Add(resultLayer[neuronIndex].Axon.Value);
                }
                return results;
            }
        }

        /// <summary>
        /// Neural Network Layers
        /// </summary>
        public List<Layer> Layers { get; private set; }

        // Constructor(s)

        public Network(Topology topology)
        {
            Topology = topology;

            // Instanciating a new network
            Layers = new List<Layer>();

            // Creating the layers
            CreateLayers(topology);

            // Define the activation & derivative function
            MethodInfo activationMethod = typeof(ActivationFunctions).GetMethod(ActivationFunctionName, BindingFlags.Public | BindingFlags.Static);
            MethodInfo derivativeMethod = typeof(ActivationFunctions).GetMethod("Derivative"+ActivationFunctionName, BindingFlags.Public | BindingFlags.Static);

            ActivationFunction = (Function)Delegate.CreateDelegate(typeof(Function), activationMethod);
            DerivativeFunction = (Function)Delegate.CreateDelegate(typeof(Function), derivativeMethod);
        }

        public Network(Topology topology, double learnRate, double momentum) : this(topology)
        {
            LearnRate = learnRate;
            Momentum = momentum;
        }

        public Network(Topology topology, double learnRate, double momentum, int maxEpochs) : this(topology, learnRate, momentum)
        {
            MaxEpochs = maxEpochs;
        }

        public Network(Topology topology, double learnRate, double momentum, int maxEpochs, double targetedAverageError) : this(topology, learnRate, momentum, maxEpochs)
        {
            TargetedAverageError = targetedAverageError;
        }

        public Network(Topology topology, double learnRate, double momentum, int maxEpochs, double targetedAverageError, string activationFunctionName) : this(topology, learnRate, momentum, maxEpochs, targetedAverageError)
        {
            ActivationFunctionName = activationFunctionName;
        }

        // Method(s)

        /// <summary>
        /// Create layers from given topology
        /// </summary>
        /// <param name="topology">The neurons's count by layer</param>
        private void CreateLayers(Topology topology)
        {
            int topologySize = topology.Count();
            // Creating the layers
            for (int layerIndex = 0; layerIndex < topologySize; layerIndex++)
            {
                // Instanciating a new layer
                int neuronsCount = topology[layerIndex];
                Layer layer = new Layer(this, layerIndex, neuronsCount);

                // Force the bias node's output value to 1.0 (the last neuron from the layer)
                layer[layer.Count - 1].Axon.Value = 1.0;

                // Adding the layer in the network
                Layers.Add(layer);
            }
        }

        /// <summary>
        /// Set the inputs of the neural network and forward the values through the network
        /// </summary>
        /// <param name="inputs">Input values</param>
        public void FeedForward(List<double> inputs)
        {
            // Asserting the input size validity
            if (inputs.Count != Layers[0].Count - 1)
            {
                throw new ArgumentException("the inputs size is not valid", "inputs");
            }

            // Feed the input neurons
            Feed(inputs);

            // Triggering the Forward Propagation
            ForwardPropagation();
        }

        /// <summary>
        /// Feed the input neurons with the given inputs
        /// </summary>
        /// <param name="inputs">Input values</param>
        private void Feed(List<double> inputs)
        {

            // Assigning the inputs to the input neurons
            for (int index = 0; index < inputs.Count; index++)
            {
                Neuron neuron = Layers[0][index];
                neuron.Axon.Value = inputs[index];
            }
        }

        /// <summary>
        /// Trigger the Forward Propagation on the neural network
        /// </summary>
        private void ForwardPropagation()
        {
            for (int layerIndex = 1; layerIndex < Layers.Count; layerIndex++)
            {
                for (int neuronIndex = 0; neuronIndex < Layers[layerIndex].Count - 1; neuronIndex++)
                {
                    Layers[layerIndex][neuronIndex].ForwardPropagation();
                }
            }
        }

        /// <summary>
        /// Trigger the Backpropagation on the neural network
        /// </summary>
        /// <param name="expectedValues">Expected values</param>
        public void BackPropagation(List<double> expectedValues)
        {
            Layer outputLayer = Layers[Layers.Count - 1];

            // Update the network average error
            UpdateRecentAverageError(outputLayer, expectedValues);


            // Output Layer Backpropagation
            // Calculate output layer gradients
            for (int neuronIndex = 0; neuronIndex < outputLayer.Count - 1; neuronIndex++)
            {
                double expected = expectedValues[neuronIndex];
                outputLayer[neuronIndex].Axon.BackPropagation(expected);
            }


            // Hidden Layer Backpropagation
            // Calculate hiden layers gradients
            for (int layerIndex = Layers.Count - 2; layerIndex > 0; layerIndex--)
            {
                Layer hiddenLayer = Layers[layerIndex];
                Layer nextLayer = Layers[layerIndex + 1];

                for (int neuronIndex = 0; neuronIndex < hiddenLayer.Count; neuronIndex++)
                {
                    hiddenLayer[neuronIndex].Axon.BackPropagation();
                }
            }


            // Update connection weights
            // For all layers from outputs to first hidden layer
            for (int layerIndex = Layers.Count - 1; layerIndex > 0; layerIndex--)
            {
                Layer layer = Layers[layerIndex];

                for (int neuronIndex = 0; neuronIndex < layer.Count - 1; neuronIndex++)
                {
                    layer[neuronIndex].UpdateDendritesWeight();
                }
            }
        }

        /// <summary>
        /// Calculate and update the overall neural network average error
        /// </summary>
        /// <param name="outputLayer"></param>
        /// <param name="expectedValues"></param>
        private void UpdateRecentAverageError(Layer outputLayer, List<double> expectedValues)
        {
            // Calculate RMS & Update RecentAverageError
            // RMS = Root Mean Square Error (Root Square from average square of global value on period)
            // RMS = rsqr( 1/n * sum (target[i] - actual[i])² )
            // RMS of output neuron errors

            double error = 0.0;

            // sum square delta
            for (int neuronIndex = 0; neuronIndex < outputLayer.Count - 1; neuronIndex++)
            {
                double expected = expectedValues[neuronIndex];
                double delta = expected - outputLayer[neuronIndex].Axon.Value;
                error += delta * delta;
            }

            // 1/n : average error squared
            error /= outputLayer.Count - 1;

            // RMS
            error = Math.Sqrt(error);

            // Update the recent average measurement
            RecentAverageError = (RecentAverageError * recentAverageSmoothingFactor + error)
                                    / (recentAverageSmoothingFactor + 1.0);

        }
    }
}
