﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyNeuralNetwork.Classes
{
    class Neuron
    {
        // Properties

        /// <summary>
        /// Index of the neuron in the layer
        /// </summary>
        public int Index { get; private set; }

        /// <summary>
        /// Internal value of the neuron
        /// </summary>
        public double InternalValue { get; private set; }

        // Link(s)

        /// <summary>
        /// The neuron axon
        /// </summary>
        public Axon Axon { get; private set; }  // main output of the neuron

        /// <summary>
        ///  The neuron list of dendrites
        /// </summary>
        public List<Dendrite> Dendrites { get; private set; } // links with input axons

        /// <summary>
        ///  The neuron layer
        /// </summary>
        public Layer Layer { get; private set; } // links with network layer

        // Constructor(s)

        public Neuron(int index, Layer layer)
        {
            Console.WriteLine("Creating a new Neuron");

            Index = index;
            Axon = new Axon(this);
            Dendrites = new List<Dendrite>();
            Layer = layer;
        }

        public Neuron(int index, Layer layer, List<Neuron> linkedNeurons) : this(index, layer)
        {
            // Creating the dendrites
            // and linking them to the axons from the previous layer
            for (int neuronIndex = 0; neuronIndex < linkedNeurons.Count; neuronIndex++)
            {
                Axon linkedAxon = linkedNeurons[neuronIndex].Axon;

                // Instanciate the new dendrite
                Dendrite dendrite = new Dendrite(this, linkedAxon);

                // Linking the dendrite to his owner neuron
                this.Dendrites.Add(dendrite);

                // Linking the dendrite to his linked axon
                linkedAxon.Dendrites.Add(dendrite);
            }
        }

        // Methods

        /// <summary>
        /// Update the dendrite's weight
        /// </summary>
        public void UpdateDendritesWeight()
        {
            Dendrites.ForEach(dendrite => dendrite.UpdateWeight());
        }

        /// <summary>
        /// Get the values from the dendrites and forward the weighted sum to the axon by the activation function
        /// </summary>
        public void ForwardPropagation()
        {
            // Internal value of the neuron is the weighted sum of the dendrites incomming values

            // Sum the values incoming from our dendrites
            InternalValue = Dendrites.Sum(dendrite => dendrite.WeightedValue);

            // Forward the value
            Axon.Value = Layer.Network.ActivationFunction(InternalValue);
        }
    }
}
