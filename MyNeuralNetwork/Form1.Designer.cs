﻿namespace MyNeuralNetwork
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnStart = new System.Windows.Forms.Button();
            this.grpTrainingConfiguration = new System.Windows.Forms.GroupBox();
            this.btnLoad = new System.Windows.Forms.Button();
            this.numTargetedAverageError = new System.Windows.Forms.NumericUpDown();
            this.numMaxEpochs = new System.Windows.Forms.NumericUpDown();
            this.lblTargetedAverageError = new System.Windows.Forms.Label();
            this.lblMaxEpochs = new System.Windows.Forms.Label();
            this.btnResume = new System.Windows.Forms.Button();
            this.lblEpoch = new System.Windows.Forms.Label();
            this.txtEpoch = new System.Windows.Forms.TextBox();
            this.txtAverageError = new System.Windows.Forms.TextBox();
            this.lblAverageError = new System.Windows.Forms.Label();
            this.btnStop = new System.Windows.Forms.Button();
            this.grpTest = new System.Windows.Forms.GroupBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.txtOutputs = new System.Windows.Forms.TextBox();
            this.lblExpected = new System.Windows.Forms.Label();
            this.btnLearn = new System.Windows.Forms.Button();
            this.btnTest = new System.Windows.Forms.Button();
            this.lblOutput = new System.Windows.Forms.Label();
            this.lblInput = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.lblTitle = new System.Windows.Forms.Label();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.grpNetworkConfiguration = new System.Windows.Forms.GroupBox();
            this.grpTraining = new System.Windows.Forms.GroupBox();
            this.lblTopology = new System.Windows.Forms.Label();
            this.mskInputs = new System.Windows.Forms.MaskedTextBox();
            this.mskExpectedValues = new System.Windows.Forms.MaskedTextBox();
            this.txtTopology = new System.Windows.Forms.TextBox();
            this.grpTrainingConfiguration.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTargetedAverageError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxEpochs)).BeginInit();
            this.grpTest.SuspendLayout();
            this.grpNetworkConfiguration.SuspendLayout();
            this.grpTraining.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.Enabled = false;
            this.btnStart.Location = new System.Drawing.Point(6, 19);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(69, 23);
            this.btnStart.TabIndex = 1;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // grpTrainingConfiguration
            // 
            this.grpTrainingConfiguration.Controls.Add(this.numTargetedAverageError);
            this.grpTrainingConfiguration.Controls.Add(this.numMaxEpochs);
            this.grpTrainingConfiguration.Controls.Add(this.lblTargetedAverageError);
            this.grpTrainingConfiguration.Controls.Add(this.lblMaxEpochs);
            this.grpTrainingConfiguration.Location = new System.Drawing.Point(7, 71);
            this.grpTrainingConfiguration.Name = "grpTrainingConfiguration";
            this.grpTrainingConfiguration.Size = new System.Drawing.Size(199, 71);
            this.grpTrainingConfiguration.TabIndex = 7;
            this.grpTrainingConfiguration.TabStop = false;
            this.grpTrainingConfiguration.Text = "Training Configuration";
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(106, 148);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(100, 37);
            this.btnLoad.TabIndex = 8;
            this.btnLoad.Text = "Load Network Configuration File";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // numTargetedAverageError
            // 
            this.numTargetedAverageError.DecimalPlaces = 5;
            this.numTargetedAverageError.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numTargetedAverageError.Location = new System.Drawing.Point(99, 45);
            this.numTargetedAverageError.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numTargetedAverageError.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            327680});
            this.numTargetedAverageError.Name = "numTargetedAverageError";
            this.numTargetedAverageError.Size = new System.Drawing.Size(94, 20);
            this.numTargetedAverageError.TabIndex = 4;
            this.numTargetedAverageError.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numTargetedAverageError.Value = new decimal(new int[] {
            9,
            0,
            0,
            131072});
            // 
            // numMaxEpochs
            // 
            this.numMaxEpochs.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numMaxEpochs.Location = new System.Drawing.Point(99, 19);
            this.numMaxEpochs.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numMaxEpochs.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numMaxEpochs.Name = "numMaxEpochs";
            this.numMaxEpochs.Size = new System.Drawing.Size(94, 20);
            this.numMaxEpochs.TabIndex = 2;
            this.numMaxEpochs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numMaxEpochs.Value = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            // 
            // lblTargetedAverageError
            // 
            this.lblTargetedAverageError.AutoSize = true;
            this.lblTargetedAverageError.Location = new System.Drawing.Point(6, 48);
            this.lblTargetedAverageError.Name = "lblTargetedAverageError";
            this.lblTargetedAverageError.Size = new System.Drawing.Size(91, 13);
            this.lblTargetedAverageError.TabIndex = 3;
            this.lblTargetedAverageError.Text = "Target Avg Error :";
            // 
            // lblMaxEpochs
            // 
            this.lblMaxEpochs.AutoSize = true;
            this.lblMaxEpochs.Location = new System.Drawing.Point(6, 22);
            this.lblMaxEpochs.Name = "lblMaxEpochs";
            this.lblMaxEpochs.Size = new System.Drawing.Size(72, 13);
            this.lblMaxEpochs.TabIndex = 1;
            this.lblMaxEpochs.Text = "Max Epochs :";
            // 
            // btnResume
            // 
            this.btnResume.Enabled = false;
            this.btnResume.Location = new System.Drawing.Point(156, 19);
            this.btnResume.Name = "btnResume";
            this.btnResume.Size = new System.Drawing.Size(69, 23);
            this.btnResume.TabIndex = 3;
            this.btnResume.Text = "Resume";
            this.btnResume.UseVisualStyleBackColor = true;
            this.btnResume.Click += new System.EventHandler(this.btnResume_Click);
            // 
            // lblEpoch
            // 
            this.lblEpoch.AutoSize = true;
            this.lblEpoch.Location = new System.Drawing.Point(6, 51);
            this.lblEpoch.Name = "lblEpoch";
            this.lblEpoch.Size = new System.Drawing.Size(44, 13);
            this.lblEpoch.TabIndex = 4;
            this.lblEpoch.Text = "Epoch :";
            // 
            // txtEpoch
            // 
            this.txtEpoch.Location = new System.Drawing.Point(120, 48);
            this.txtEpoch.Name = "txtEpoch";
            this.txtEpoch.ReadOnly = true;
            this.txtEpoch.Size = new System.Drawing.Size(105, 20);
            this.txtEpoch.TabIndex = 5;
            // 
            // txtAverageError
            // 
            this.txtAverageError.Location = new System.Drawing.Point(120, 74);
            this.txtAverageError.Name = "txtAverageError";
            this.txtAverageError.ReadOnly = true;
            this.txtAverageError.Size = new System.Drawing.Size(105, 20);
            this.txtAverageError.TabIndex = 7;
            // 
            // lblAverageError
            // 
            this.lblAverageError.AutoSize = true;
            this.lblAverageError.Location = new System.Drawing.Point(6, 77);
            this.lblAverageError.Name = "lblAverageError";
            this.lblAverageError.Size = new System.Drawing.Size(78, 13);
            this.lblAverageError.TabIndex = 6;
            this.lblAverageError.Text = "Average Error :";
            // 
            // btnStop
            // 
            this.btnStop.Enabled = false;
            this.btnStop.Location = new System.Drawing.Point(81, 19);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(69, 23);
            this.btnStop.TabIndex = 2;
            this.btnStop.Text = "Pause";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // grpTest
            // 
            this.grpTest.Controls.Add(this.mskExpectedValues);
            this.grpTest.Controls.Add(this.mskInputs);
            this.grpTest.Controls.Add(this.txtOutputs);
            this.grpTest.Controls.Add(this.lblExpected);
            this.grpTest.Controls.Add(this.btnLearn);
            this.grpTest.Controls.Add(this.btnTest);
            this.grpTest.Controls.Add(this.lblOutput);
            this.grpTest.Controls.Add(this.lblInput);
            this.grpTest.Location = new System.Drawing.Point(230, 112);
            this.grpTest.Name = "grpTest";
            this.grpTest.Size = new System.Drawing.Size(231, 91);
            this.grpTest.TabIndex = 3;
            this.grpTest.TabStop = false;
            this.grpTest.Text = "Test";
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(386, 209);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 4;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // txtOutputs
            // 
            this.txtOutputs.Location = new System.Drawing.Point(69, 42);
            this.txtOutputs.Margin = new System.Windows.Forms.Padding(2);
            this.txtOutputs.Name = "txtOutputs";
            this.txtOutputs.ReadOnly = true;
            this.txtOutputs.Size = new System.Drawing.Size(76, 20);
            this.txtOutputs.TabIndex = 5;
            // 
            // lblExpected
            // 
            this.lblExpected.AutoSize = true;
            this.lblExpected.Location = new System.Drawing.Point(6, 68);
            this.lblExpected.Name = "lblExpected";
            this.lblExpected.Size = new System.Drawing.Size(58, 13);
            this.lblExpected.TabIndex = 6;
            this.lblExpected.Text = "Expected :";
            // 
            // btnLearn
            // 
            this.btnLearn.Enabled = false;
            this.btnLearn.Location = new System.Drawing.Point(150, 64);
            this.btnLearn.Name = "btnLearn";
            this.btnLearn.Size = new System.Drawing.Size(75, 23);
            this.btnLearn.TabIndex = 8;
            this.btnLearn.Text = "Learn";
            this.btnLearn.UseVisualStyleBackColor = true;
            this.btnLearn.Click += new System.EventHandler(this.btnLearn_Click);
            // 
            // btnTest
            // 
            this.btnTest.Enabled = false;
            this.btnTest.Location = new System.Drawing.Point(150, 16);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(75, 23);
            this.btnTest.TabIndex = 3;
            this.btnTest.Text = "Test";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // lblOutput
            // 
            this.lblOutput.AutoSize = true;
            this.lblOutput.Location = new System.Drawing.Point(6, 45);
            this.lblOutput.Name = "lblOutput";
            this.lblOutput.Size = new System.Drawing.Size(45, 13);
            this.lblOutput.TabIndex = 4;
            this.lblOutput.Text = "Output :";
            // 
            // lblInput
            // 
            this.lblInput.AutoSize = true;
            this.lblInput.Location = new System.Drawing.Point(6, 21);
            this.lblInput.Name = "lblInput";
            this.lblInput.Size = new System.Drawing.Size(37, 13);
            this.lblInput.TabIndex = 1;
            this.lblInput.Text = "Input :";
            // 
            // timer1
            // 
            this.timer1.Interval = 1;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Location = new System.Drawing.Point(4, 21);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(33, 13);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "Title :";
            // 
            // txtTitle
            // 
            this.txtTitle.Location = new System.Drawing.Point(106, 18);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.ReadOnly = true;
            this.txtTitle.Size = new System.Drawing.Size(100, 20);
            this.txtTitle.TabIndex = 2;
            // 
            // grpNetworkConfiguration
            // 
            this.grpNetworkConfiguration.Controls.Add(this.txtTopology);
            this.grpNetworkConfiguration.Controls.Add(this.btnLoad);
            this.grpNetworkConfiguration.Controls.Add(this.grpTrainingConfiguration);
            this.grpNetworkConfiguration.Controls.Add(this.lblTopology);
            this.grpNetworkConfiguration.Controls.Add(this.txtTitle);
            this.grpNetworkConfiguration.Controls.Add(this.lblTitle);
            this.grpNetworkConfiguration.Location = new System.Drawing.Point(12, 12);
            this.grpNetworkConfiguration.Name = "grpNetworkConfiguration";
            this.grpNetworkConfiguration.Size = new System.Drawing.Size(212, 191);
            this.grpNetworkConfiguration.TabIndex = 1;
            this.grpNetworkConfiguration.TabStop = false;
            this.grpNetworkConfiguration.Text = "Network Configuration";
            // 
            // grpTraining
            // 
            this.grpTraining.Controls.Add(this.btnStart);
            this.grpTraining.Controls.Add(this.lblEpoch);
            this.grpTraining.Controls.Add(this.btnResume);
            this.grpTraining.Controls.Add(this.txtEpoch);
            this.grpTraining.Controls.Add(this.btnStop);
            this.grpTraining.Controls.Add(this.txtAverageError);
            this.grpTraining.Controls.Add(this.lblAverageError);
            this.grpTraining.Location = new System.Drawing.Point(230, 12);
            this.grpTraining.Name = "grpTraining";
            this.grpTraining.Size = new System.Drawing.Size(231, 100);
            this.grpTraining.TabIndex = 2;
            this.grpTraining.TabStop = false;
            this.grpTraining.Text = "Training";
            // 
            // lblTopology
            // 
            this.lblTopology.AutoSize = true;
            this.lblTopology.Location = new System.Drawing.Point(4, 47);
            this.lblTopology.Name = "lblTopology";
            this.lblTopology.Size = new System.Drawing.Size(57, 13);
            this.lblTopology.TabIndex = 3;
            this.lblTopology.Text = "Topology :";
            // 
            // mskInputs
            // 
            this.mskInputs.Location = new System.Drawing.Point(69, 18);
            this.mskInputs.Name = "mskInputs";
            this.mskInputs.Size = new System.Drawing.Size(76, 20);
            this.mskInputs.TabIndex = 2;
            // 
            // mskExpectedValues
            // 
            this.mskExpectedValues.Location = new System.Drawing.Point(69, 65);
            this.mskExpectedValues.Name = "mskExpectedValues";
            this.mskExpectedValues.Size = new System.Drawing.Size(76, 20);
            this.mskExpectedValues.TabIndex = 7;
            // 
            // txtTopology
            // 
            this.txtTopology.Location = new System.Drawing.Point(106, 44);
            this.txtTopology.Name = "txtTopology";
            this.txtTopology.ReadOnly = true;
            this.txtTopology.Size = new System.Drawing.Size(100, 20);
            this.txtTopology.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(473, 244);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.grpTraining);
            this.Controls.Add(this.grpNetworkConfiguration);
            this.Controls.Add(this.grpTest);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Neuronal Network";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.grpTrainingConfiguration.ResumeLayout(false);
            this.grpTrainingConfiguration.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTargetedAverageError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxEpochs)).EndInit();
            this.grpTest.ResumeLayout(false);
            this.grpTest.PerformLayout();
            this.grpNetworkConfiguration.ResumeLayout(false);
            this.grpNetworkConfiguration.PerformLayout();
            this.grpTraining.ResumeLayout(false);
            this.grpTraining.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.GroupBox grpTrainingConfiguration;
        private System.Windows.Forms.Label lblTargetedAverageError;
        private System.Windows.Forms.Label lblMaxEpochs;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.GroupBox grpTest;
        private System.Windows.Forms.Label lblOutput;
        private System.Windows.Forms.Label lblInput;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.NumericUpDown numTargetedAverageError;
        private System.Windows.Forms.NumericUpDown numMaxEpochs;
        private System.Windows.Forms.TextBox txtAverageError;
        private System.Windows.Forms.Label lblAverageError;
        private System.Windows.Forms.Label lblEpoch;
        private System.Windows.Forms.TextBox txtEpoch;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnResume;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label lblExpected;
        private System.Windows.Forms.Button btnLearn;
        private System.Windows.Forms.TextBox txtOutputs;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.GroupBox grpNetworkConfiguration;
        private System.Windows.Forms.GroupBox grpTraining;
        private System.Windows.Forms.Label lblTopology;
        private System.Windows.Forms.MaskedTextBox mskExpectedValues;
        private System.Windows.Forms.MaskedTextBox mskInputs;
        private System.Windows.Forms.TextBox txtTopology;
    }
}

