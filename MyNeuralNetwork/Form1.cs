﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyNeuralNetwork.Classes;
using MyNeuralNetwork.Tools;


namespace MyNeuralNetwork
{
    public partial class Form1 : Form
    {
        NetworkConfiguration netConfig;
        Network net;
        int epoch;
        int maxEpochs;
        double targetError;
        double error;
        int validationDataIndex;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //btnLoad_Click(this, EventArgs.Empty);
            //NetworkConfiguration.CreateConfigurationFileExample();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            DialogResult result;

            // Configuring the open file dialog window

            // File Filter
            openFileDialog1.Filter = "Fichiers XML (*.xml)|*.xml|Tous les Fichiers (*.*)|*.*";

            // Initial Directory
            openFileDialog1.InitialDirectory = Application.StartupPath;
            //openFileDialog1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);

            // Initial filename
            openFileDialog1.FileName = "";

            result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                string path = openFileDialog1.FileName;

                btnResume.Enabled = false;
                btnStart.Enabled = true;
                btnTest.Enabled = true;
                btnLearn.Enabled = true;
                txtEpoch.Text = "";
                txtAverageError.Text = "";

                netConfig = NetworkConfiguration.LoadConfigurationFile(path);

                //TrainingData training = new TrainingData(filename);

                //
                // General Neural Network Configuration
                //

                txtTitle.Text = netConfig.Title;

                // Reading neural network topology
                // it's the neurons's count by layer
                // for example : 4,3,3
                // that means :
                //  * 4 neurons in the input layer (first layer)
                //  * 3 neurons in the hidden layer (hidden layer is facultative)
                //  * 3 neurons in the output layer (last layer)
                Topology topology = netConfig.Topology;

                // Updating topology display
                string topologyString = "";
                for(int i = 0; i < topology.Count; i++)
                {
                    topologyString += topology[i] + ", ";
                }
                topologyString = topologyString.Substring(0, topologyString.Length - 2);
                txtTopology.Text = topologyString;

                // Updating input mask
                string mask = "";
                for(int i = 0; i < topology[0]; i++)
                {
                    mask += @"0\, ";
                }
                mask = mask.Substring(0, mask.Length - 3);
                mskInputs.Mask = mask;

                // Updating expected mask
                mask = "";
                for (int i = 0; i < topology[topology.Count - 1]; i++)
                {
                    mask += @"0\, ";
                }
                mask = mask.Substring(0, mask.Length - 3);
                mskExpectedValues.Mask = mask;

                // Learning Rate
                // 0.0 : no learning
                // 0.1 : slow learner (recommanded)
                // 0.2 : medium learner
                // 1.0 : reckless learner
                double learnRate = netConfig.LearnRate;

                // Momentum
                // Multiplier for last weight change [0.0..n]
                // 0.0 : no momentum
                // 0.5 : moderate momentum (recommanded)
                double momentum = netConfig.Momentum;

                maxEpochs = netConfig.TrainingConfiguration.MaxEpochs;
                numMaxEpochs.Value = maxEpochs;

                double targetedAverageError = netConfig.TrainingConfiguration.TargetedAverageError;
                numTargetedAverageError.Value = Convert.ToDecimal(targetedAverageError);

                string activationFunctionName = netConfig.ActivationFunctionName;

                // Creating the neural network
                net = new Network(topology, learnRate, momentum, maxEpochs, targetedAverageError, activationFunctionName);
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            epoch = 1;
            error = 1;
            validationDataIndex = 0;

            maxEpochs = (int)numMaxEpochs.Value;
            targetError = (double)numTargetedAverageError.Value;

            btnLoad.Enabled = false;
            btnStart.Enabled = false;
            btnStop.Enabled = true;
            btnResume.Enabled = false;

            numMaxEpochs.Enabled = false;
            numTargetedAverageError.Enabled = false;

            timer1.Start();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            btnLoad.Enabled = true;
            btnStart.Enabled = true;
            btnStop.Enabled = false;
            btnResume.Enabled = true;

            numMaxEpochs.Enabled = true;
            numTargetedAverageError.Enabled = true;

            timer1.Stop();
        }

        private void btnResume_Click(object sender, EventArgs e)
        {
            maxEpochs = (int)numMaxEpochs.Value;
            targetError = (double)numTargetedAverageError.Value;

            btnLoad.Enabled = false;
            btnStart.Enabled = false;
            btnStop.Enabled = true;
            btnResume.Enabled = false;

            numMaxEpochs.Enabled = false;
            numTargetedAverageError.Enabled = false;

            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            List<double> inputs, expectedValues, results;

            // Training
            if (epoch <= maxEpochs && (error >= targetError || epoch <= maxEpochs * 50 / 100))
            {
                txtEpoch.Text = epoch.ToString();
                Console.WriteLine("Epoch: " + epoch);
                // Get the new input data and feed it forward
                inputs = netConfig.TrainingConfiguration.ValidationDataSet[validationDataIndex].InputValues;
                //inputs = inputList[trainingSetIndex];
                ShowList(" - Inputs: ", inputs);
                net.FeedForward(inputs);

                // Get the network results
                results = net.Results;
                ShowList(" - Results: ", results);

                // Train the network what the outputs should have been
                expectedValues = netConfig.TrainingConfiguration.ValidationDataSet[validationDataIndex].ExpectedOutputValues;
                //expectedValues = expectedList[trainingSetIndex];
                ShowList(" - Expected: ", expectedValues);
                net.BackPropagation(expectedValues);

                // Report how well the training is working, averaged over recent
                error = net.RecentAverageError;
                txtAverageError.Text = error.ToString();
                Console.WriteLine("Network average error : " + net.RecentAverageError);

                epoch++;
                validationDataIndex = (validationDataIndex + 1) % netConfig.TrainingConfiguration.ValidationDataSet.Count;
            }
            else
            {
                Console.WriteLine("Training Done.");
                btnStop_Click(this, EventArgs.Empty);
            }

        }

        private void ShowList(string label, List<double> list)
        {
            Console.WriteLine(label + String.Join(", ", list));
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            List<double> inputs, results;
            inputs = new List<double>();
            List<string> inputsString = mskInputs.Text.Split(',').ToList();

            if (inputsString.Count != net.Topology[0])
            {
                MessageBox.Show("Invalid input format");
            }
            else
            {
                inputsString.ForEach(input => inputs.Add(double.Parse(input)));
                ShowList(" Test - Inputs: ", inputs);
                net.FeedForward(inputs);
                results = net.Results;
                ShowList(" Test - Results: ", results);
                for (int index = 0; index < results.Count; index++)
                {
                    results[index] = Math.Round(results[index]);
                }
                txtOutputs.Text = String.Join(",", results);
            }
        }

        private void btnLearn_Click(object sender, EventArgs e)
        {
            btnTest_Click(this, e);
            List<double> expectedValues;

            expectedValues = new List<double>();
            List<string> expectedValuesString = mskExpectedValues.Text.Split(',').ToList();

            if (expectedValuesString.Count != net.Topology[net.Topology.Count - 1])
            {
                MessageBox.Show("Invalid expected format");
            }
            else
            {
                expectedValuesString.ForEach(expectedValue => expectedValues.Add(double.Parse(expectedValue)));

                ShowList(" - Expected: ", expectedValues);
                net.BackPropagation(expectedValues);

                error = net.RecentAverageError;
                txtAverageError.Text = error.ToString();
                Console.WriteLine("Network average error : " + net.RecentAverageError);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
