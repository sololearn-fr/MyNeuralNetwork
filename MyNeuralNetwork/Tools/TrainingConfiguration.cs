﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyNeuralNetwork.Tools
{
    public class TrainingConfiguration
    {
        public int MaxEpochs { get; set; }
        public double TargetedAverageError { get; set; }
        public ValidationDataSet ValidationDataSet { get; set; }
    }
}
